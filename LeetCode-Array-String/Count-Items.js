const items = [["phone", "blue", "pixel"], ["computer", "silver", "lenovo"], ["phone", "gold", "iphone"]];
const ruleKey = "color", ruleValue = "silver";
let countMatches = function (items, ruleKey, ruleValue) {
    let matches = 0;
    switch (ruleKey) {
        case "color": searchIndex = 1; break;
        case "type": searchIndex = 0; break;
        case "name": searchIndex = 2; break;
    }
    for (let i = 0; i < items.length; i++) {
        let currArrayIndex = items[i];
        for (let j = 0; j < currArrayIndex.length; j++) {
            if (currArrayIndex[j] === ruleValue) {
                if (searchIndex === j) {
                    matches++;
                }
            }
        }
    }
    return matches
};