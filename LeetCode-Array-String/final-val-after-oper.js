const operations = ["--X", "X++", "X++"];

const finalValueAfterOperations = function (operations) {
    let x = 0;
    for (const el of operations) {
        switch (el) {
            case "--X": x--; break;
            case "X--": x--; break;
            case "++X": x++; break;
            case "X++": x++; break;
        }
    }
    return x;
};