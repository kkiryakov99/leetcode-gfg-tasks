const words1 = ["leetcode", "is", "amazing", "as", "is"];
const words2 = ["amazing", "leetcode", "is"];
const countWords = function (words1, words2) {
    const all1 = new Set();
    const all2 = new Set();
    const toRemove = new Set();
    let count = 0;
    for (const words of words1) {
        if (!all1.has(words)) {
            all1.add(words);
        } else {
            toRemove.add(words);
        }
    }
    for (const el of words2) {
        if (!all2.has(el)) {
            all2.add(el);
        } else {
            toRemove.add(el);
        }
    }
    for (const element of all1) {
        if (toRemove.has(element)) {
            all1.delete(element);
        }
    }
    for (const element2 of all2) {
        if (toRemove.has(element2)) {
            all2.delete(element2);
        }
    }
    for (const w1 of all1) {
        if (all2.has(w1)) {
            count++;
        }
    }
    return count;
};