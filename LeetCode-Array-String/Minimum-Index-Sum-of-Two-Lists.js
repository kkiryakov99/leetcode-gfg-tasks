const list1 = ["Shogun", "Tapioca Express", "Burger King", "KFC"];
const list2 = ["Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"];

let findRestaurant = function (list1, list2) {
    let answer = [];
    let allSums = [];
    let final = [];
    let min = Number.MAX_SAFE_INTEGER;
    for (const rest1 of list1) {
        let currSum = 0;
        for (const rest2 of list2) {
            if (rest1 === rest2) {
                currSum = list1.indexOf(rest1) + list2.indexOf(rest2);
                answer.push(rest2);
                allSums.push(currSum);
                if (currSum < min) {
                    min = currSum;
                }
            }
        }
    }
    if (answer.length === 1) {
        return answer;
    } else {
        for (let i = 0; i < allSums.length; i++) {
            if (allSums[i] <= min) {
                final.push(answer[i])
            }
        }
    }
    return final;
};