const arr = ["d", "b", "c", "b", "c", "a"];
const k = 2;
var kthDistinct = function (arr, k) {
    let array = [];
    for (const el of arr) {
        if (arr.indexOf(el) === arr.lastIndexOf(el)) {
            array.push(el);
        }
    }
    let a = array[k - 1];
    if (a === undefined) {
        return "";
    } else {
        return a;
    }
};